#include <stdio.h>	// printf and fprintf
#include <math.h>	// pow
#include <getopt.h>	// getopt
#include <stdlib.h>	// atoi

void usage() {
	fprintf ( stderr, "Usage: openchord [-x] [-f number] <string1_pitch> <string2_pitch> ... \n" );
}

int distance ( int a, int b ){ // calculate the shortest distance between two notes
	int distance;
	distance = (a-b+12)%12;
	if ( distance > 12-distance ) distance = 12-distance;
	return distance;
}

int interval ( int a, int b){ // calculate the ascending interval between two notes
	int interval;
	interval = (b-a+12)%12;
	return interval;
}

char* numptostr ( int a ) {
	switch ( a ) { // convert numerical pitch to letter note names
		case 0:
			return ("C");
			break;
		case 1:
			return ("C#/Db");
			break;
		case 2:
			return ("D");
			break;
		case 3:
			return ("D#/Eb");
			break;
		case 4:
			return ("E");
			break;
		case 5:
			return ("F");
			break;
		case 6:
			return ("F#/Gb");
			break;
		case 7:
			return ("G");
			break;
		case 8:
			return ("G#/Ab");
			break;
		case 9:
			return ("A");
			break;
		case 10:
			return ("A#/Bb");
			break;
		case 11:
			return ("B");
			break;
		default :
			fprintf ( stderr, "\nError: failed to convert numerical pitch to string.\n" );
			fprintf ( stderr, "This should never happen.\n" );
			exit(1);
	}
}

int main ( int argc, char *argv[] ) {
	int maxfret = 4; // highest fret allowed in chords TODO: assign this a flag (maybe -f?)
	int opt;
	int x = 0;
	while ( ( opt = getopt ( argc, argv, "xf:" ) ) != -1 ){
		switch ( opt ) {
			case 'f' :
				maxfret = atoi ( optarg ) + 1;
				break;
			case 'x' :
				x = 1;
				break;
			default :
				usage();
				return 1;
		}
	}

	if ( argc <= optind ) {
		fprintf ( stderr, "Error: too few arguments" );
		usage();
		return 1;
	}

	int n = argc - optind;
	int strings[ n ];
	for ( int i=0; i < n; i++ ) {	// convert arguments to numerical pitch
		switch ( argv[i+optind][0] ) {
			case 'C' :
			case 'c' :
				strings[i]=0;
				break;
			case 'D' :
			case 'd' :
				strings[i]=2;
				break;
			case 'E' :
			case 'e' :
				strings[i]=4;
				break;
			case 'F' :
			case 'f' :
				strings[i]=5;
				break;
			case 'G' :
			case 'g' :
				strings[i]=7;
				break;
			case 'A' :
			case 'a' :
				strings[i]=9;
				break;
			case 'B' :
			case 'b' :
				strings[i]=11;
				break;
			default :
				fprintf ( stderr, "Error: bad argument: %s\n", argv[i+optind] );
				usage();
				return 1;
		}

		switch ( argv[i+optind][1] ) {
			case '\0' :
				break;
			case 'b' :
				strings[i] = (strings[i] -1);
				break;
			case '#' :
				strings[i] = (strings[i] +1);
				break;
			default :
				fprintf ( stderr, "Error: bad argument: %s\n", argv[i+optind] );
				usage();
				return 1;
		}
	}

	for ( int i=0; i < (int) pow( (double) maxfret, (double) n ) ; i++ ) {	// calculate chords
		int m = i;
		int fingering[n];
		int voicing[n];
		int openstr = 0;
		int ncts = 0;
		int pfourth = 0;
		int root = -1;
		char quality[6] = "";
		for ( int j=0; j<n; j++ ) {	// generate all possible chords
			fingering[j] = m%maxfret;
			voicing[j] = (fingering[j]+strings[j])%12;
			m/=maxfret;
			if ( fingering[j] == 0 ) openstr++;	// check for open strings
		}

		for ( int j=1; j<n; j++ ) {	// count non-chord tones
			for ( int k=0; k<j; k++ ) {
				int intv = distance ( voicing[j], voicing[k] );
				if ( (intv < 3 && intv != 0) ) ncts++;	// only allow tertian harmony
				if ( intv == 5 ) pfourth = 1;	// check for P4/P5
			}
		}

		int spelling[4] = { voicing[0] , -1, -1, -1, };
		if ( n - openstr < 4 && ncts == 0 && ( pfourth == 1 || x == 1 ) ) { // remove invalid chords : no more than three fretted strings, and only tertian harmonies, remove Aug and Dim chords unless -x flag is used.
			{
				int k = 1;
				for ( int j = 1; j<n; j++ ) {	// calculate chord spelling
					int included = 0;
					for ( int l = ( k - 1); l >=0; l-- ) {
						if ( voicing[j] == spelling[l] ) included++;
					}

					if ( included == 0 ) {
						spelling[k] = voicing[j];
						k++;
					}
				}
			}

			if ( pfourth == 1 ) {	//convert spelling to chord symbol
				for ( int j=1; j<3; j++ ) {	// calculate root
					for ( int k=0; k<j; k++ ) {
						switch ( ( 12 + spelling[j] - spelling [k] ) %12 ) {
							case 5:
								root = spelling[j];
								break;
							case 7:
								root = spelling[k];
								break;
						}
					}
				}

				for ( int j=0; j<3; j++ ) {	// calculate quality
					if ( distance ( spelling[j], root ) == 3) quality[0] = 'm';
				}
			} else { // -x chord
				if ( spelling[3] != -1 ){ // 4 notes -> dim7
					root = spelling[0];
					quality[0] = ' ';
					quality[1] = 'd';
					quality[2] = 'i';
					quality[3] = 'm';
					quality[4] = '7';
				} else if ( distance ( spelling[0], spelling [1] ) == 4 && distance ( spelling[0], spelling[2] ) == 4 ) { //all major thirds -> Aug
					root = spelling[0];
					quality[0] = ' ';
					quality[1] = 'A';
					quality[2] = 'u';
					quality[3] = 'g';
				} else { // dim triad
					int tt[2];
					int third;
					if ( distance ( spelling[0], spelling[1] ) == 6 ) {
						tt[0] = 0;
						tt[1] = 1;
						third = 2;
					} else if ( distance ( spelling[0], spelling[2] ) == 6 ) {
						tt[0] = 0;
						tt[1] = 2;
						third = 1;
					} else /* if ( distance ( spelling[1], spelling[2] ) == 6 ) */ {
						tt[0] = 1;
						tt[1] = 2;
						third = 0;
					}
					if ( interval ( tt[0], third ) == 3 ) {
						root = spelling[tt[1]];
					} else {
						root = spelling[tt[0]];
					}
					quality[0] = ' ';
					quality[1] = 'd';
					quality[2] = 'i';
					quality[3] = 'm';
				}
			}

			if ( spelling[2] > -1 ) {
				for ( int j=0; j<n; j++ ) {
					printf ( "%d ", fingering[j] );
				}

				printf ( "\t" );
				printf ( "%s", numptostr ( root == -1? spelling[0] : root ) );
				printf ( "%s", quality );
				printf ( "\e[%dG", 2*n+20 );
				for ( int j=0; j<n; j++ ) {
					printf ( "%s ", numptostr ( voicing[j] ) );
				}

				printf ( "\n" );
			}
		}
	}

	return 0;
}
