# TODO
- [x] Make something that works
- [ ] ~~Check chords that don't use all strings~~ *can be done manually by user*
- [x] Accept lowercase note spellings
- [x] Add -f flag for maxfret variable
- [x] Add flag to add Aug and Dim chords
- [x] Print chord symbol
- [ ] align tables to tabstops
