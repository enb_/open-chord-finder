openchord: openchord.c
	gcc -O2 -o openchord openchord.c -lm

install: openchord
	[ -d "/opt/open-chord-finder" ] || mkdir /opt/open-chord-finder
	cp openchord /opt/open-chord-finder
	ln -s /opt/open-chord-finder/openchord /usr/local/bin/openchord

uninstall:
	rm /usr/local/bin/openchord
	rm -r /opt/open-chord-finder/

clean:
	[ -e openchord ] && rm openchord || printf ""
