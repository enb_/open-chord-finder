# Open Chord Finder

A simple C program to find candidate open chord shapes in any tuning.

## Usage
```
openchord [-x] [-f number] string_pitch ...

-f number
        Highest fret allowed. Default 4.\
-x
        Allow Aug, dim, and dim7 chords.
```
String pitches must be capitalized, using `#` for sharp and `b` for flat.
Some shells may require `#` to be escaped.

Examples (using zsh):
```
$ openchord E G\# E G\# B E     # open E v1
```
```
$ openchord E B E "G#" B E      # open E v2
```

## Installation

Note: installation instructions are for Unix-like systems.
For those using other systems, such as **MicroSoft Windows**, the following instructions __***WILL NOT WORK***__.
To install on **Windows**, compile manually using your preferred C compiler.

### Dependencies:

Make sure you have ***all*** of these before installing!
- gcc
- POSIX-compliant coreutils, eg, Unix, BSD, GNU, BusyBox, &c

Note: non-POSIX shells (eg fish) not tested

### Compile:
```
$ make
```
### Install:
```
# make install
```
Note: will automatically compile if necessary
### Uninstall:
```
# make uninstall
```
### Update:
```
$ git pull
# make uninstall clean install
```
